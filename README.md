# ImageRoundedCorners
<br>1、这是一个对图像进行圆角处理的程序，默认输出.png格式，
<br>2、圆角的背景可以选择透明和白色两种
<br>3、可以自定义圆角的尺寸，以像素为单位
<br>4、实现思路如图：
<br><img src="https://gitee.com/writer/ImageRoundedCorners/raw/master/thinking.png" width="650" height="600" alt="图片加载失败时，显示这段字"/>
<br>5、预览图：
<br><img src="https://gitee.com/writer/ImageRoundedCorners/raw/master/preview.jpg" width="934" height="613" alt="图片加载失败时，显示这段字"/>
